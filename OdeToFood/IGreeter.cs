﻿using Microsoft.Extensions.Configuration;

namespace OdeToFood
{
    public interface IGreeter
    {
        string GetMessageOfTheDay();
    }

    public class Greeter : IGreeter
    {
        private IConfiguration _Configuration;

        public Greeter(IConfiguration configuration)
            {
                _Configuration = configuration;
            }
        
        public string GetMessageOfTheDay()
        {
            return _Configuration["Greeting"];
        }
    }
}